import { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';

import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'

import { UserProvider } from '../UserContext';

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null,
		isAdmin: null
	});

  useEffect(() => {
      setUser({
        email: localStorage.getItem('email'),
        isAdmin: localStorage.getItem('isAdmin') === 'true'   
      })
  }, [])

  const unsetUser = () => {
      localStorage.clear()

      setUser({
        email: null,
        isAdmin: null
      })
  }

  return(
  	<UserProvider value={{user, setUser, unsetUser}}>
  		<NavBar />
  		<Component {...pageProps} />
  	</UserProvider>
  )
}

export default MyApp
