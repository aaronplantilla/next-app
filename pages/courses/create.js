import { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

export default function CreateCourse() {
    //declare form input states
    const [courseId, setCourseId] = useState('');
    const [courseName, setCourseName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    //function for processing creation of a new course
    function createCourse(e) {
        e.preventDefault();

        console.log(`${courseName} with ID: ${courseId} is set to start on ${startDate} for PhP ${price} per slot.`);

        setCourseId('');
        setCourseName('');
        setDescription('');
        setPrice(0);
        setStartDate('');
        setEndDate('');
    }

    return (
        <Container>
            <Form onSubmit={(e) => createCourse(e)}>
                <Form.Group controlId="courseId">
                    <Form.Label>Course ID:</Form.Label>
                    <Form.Control type="text" placeholder="Enter course ID" value={courseId} onChange={e => setCourseId(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="courseName">
                    <Form.Label>Course Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter course name" value={courseName} onChange={e => setCourseName(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Label>Course Description:</Form.Label>
                    <Form.Control as="textarea" rows="3" placeholder="Enter course description" value={description} onChange={e => setDescription(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Course Price:</Form.Label>
                    <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="startDate">
                    <Form.Label>Start Date:</Form.Label>
                    <Form.Control type="date" value={startDate} onChange={e => setStartDate(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="endDate">
                    <Form.Label>End Date:</Form.Label>
                    <Form.Control type="date" value={endDate} onChange={e => setEndDate(e.target.value)} required/>
                </Form.Group>

                <Button variant="primary" type="submit">Submit</Button>
            </Form>
        </Container>
    )
}
