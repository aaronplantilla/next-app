export default [
    {
        email: "admin@mail.com",
        password: "admin123",
        isAdmin: true
    },
    {
        email: "john@mail.com",
        password: "john123",
        isAdmin: false
    },
    {
        email: "aaron@mail.com",
        password: "aaron123",
        isAdmin: false
    }
]